<?php 

include_once('xyz.php');

////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

function get_all_json() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_content";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK'],
            "page" => $row['PAGE'],
            "section" => $row['SECTION'],
            "comments" => $row['COMMENTS']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

function get_all_json_by_id($id) {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_people WHERE ID = $id";
    
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "fname" => $row['FNAME'],
            "lname" => $row['LNAME'],
            "dob" => $row['DOB']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

////////////////////////////////////////////////////////////////
/////////         Display all On Website          //////////////
////////////////////////////////////////////////////////////////

function showData($data, $page) {
    
    $array = json_decode($data, True);

    $today = date("Y-m-d");
    
    $output = "";

    if (count($array) > 0 ) {
        for ($i = 0; $i < count($array); $i++) {
            
            //Calculate difference between 2 dates
            $dateDiff = two_dates($today, $array[$i]['dob']);
            
            //Display date formatted
            $showDob = date("d F Y", strtotime($array[$i]['dob']));
            
            if ($page == "index") {
                //String for HTML table code
            $output .= "<tr><td>".$array[$i]['fname']."</td><td>".$array[$i]['lname']."</td><td>".$showDob."</td><td class='age'>".$dateDiff."</td></tr>";
            }
            
            if ($page == "admin") {
                //String for HTML table code
                $output .= "<tr><td>".$array[$i]['fname']."</td><td>".$array[$i]['lname']."</td><td>".$showDob."</td><td class='age'>".$dateDiff."</td><td><a href=\"edit.php?id=".$array[$i]['id']."\">Edit</a></td><td><a href=\"delete.php?id=".$array[$i]['id']."\">Delete</a></td></tr>";    
            }   
        }
        
        return $output;
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
        
        return $output;
    }
}

////////////////////////////////////////////////////////////////
/////////         Add Data Using JSON             //////////////
////////////////////////////////////////////////////////////////

function addRecordUsingJSON($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    //var jsonraw = [{ IdName:myData[1], Title:myData[2], Text:myData[3], Image:myData[4], Link:myData[5], Page:myData[6], Section:myData[7], Comments:myData[7]}];
    //INSERT INTO `tbl_content` VALUES (id, idName, title, text, image, link, page, section, comments);

    $idName = $db->real_escape_string($array[0]['IdName']);
    $title = $db->real_escape_string($array[0]['Title']);
    $text = $db->real_escape_string($array[0]['Text']);
    $image = $db->real_escape_string($array[0]['Image']);
    $link = $db->real_escape_string($array[0]['Link']);
    $page = $db->real_escape_string($array[0]['Page']);
    $section = $db->real_escape_string($array[0]['Section']);
    $comments = $db->real_escape_string($array[0]['Comments']);


    // $stmt = $db->prepare("INSERT INTO tbl_people (FNAME, LNAME, DOB) VALUES (?, ?, ?)");
    // $stmt->bind_param("sss", $fname, $lname, $dob2);
    $stmt = $db->prepare("INSERT INTO tbl_content (IDNAME, TITLE, TEXT, IMAGE, LINK, PAGE, SECTION, COMMENTS) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("ssssssss", $idName, $title, $text, $image, $link, $page, $section, $comments);
    $stmt->execute();
    
    print $stmt->error; //to check errors

    $result = $stmt->affected_rows;

    $stmt->close();
    $db->close();

    if ($result > 0 ) {

        $output = 1;
        return $output;
    }
    else {

        $output = 0;
        return $output;
    }
}

////////////////////////////////////////////////////////////////
/////////         EDIT Data Using JSON            //////////////
////////////////////////////////////////////////////////////////

function displayID()
{
    $id = $_GET['id'];
    $array = json_decode(get_all_json_by_id($id), True);
    return $array[0]['id'];
}

function displayFirstName()
{
    $id = $_GET['id'];
    $array = json_decode(get_all_json_by_id($id), True);
    return $array[0]['fname'];
}

function displayLastName()
{
    $id = $_GET['id'];
    $array = json_decode(get_all_json_by_id($id), True);
    return $array[0]['lname'];
}

function displayDob()
{
    $id = $_GET['id'];
    $array = json_decode(get_all_json_by_id($id), True);
    $dob   = $array[0]['dob'];
    return date("Y-m-d", strtotime($dob));
}

function editPersonWithJSON($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $idName = $db->real_escape_string($array[0]['IdName']);
    $title = $db->real_escape_string($array[0]['Title']);
    $text = $db->real_escape_string($array[0]['Text']);
    $image = $db->real_escape_string($array[0]['Image']);
    $link = $db->real_escape_string($array[0]['Link']);
    $page = $db->real_escape_string($array[0]['Page']);
    $section = $db->real_escape_string($array[0]['Section']);
    $comments = $db->real_escape_string($array[0]['Comments']);
    $id = $db->real_escape_string($array[0]['Id']);

    var_dump($id);

    //$dob2 = date("Y-m-d", strtotime($dob));

    $sql = "UPDATE tbl_content SET IDNAME='".$idName."', TITLE='".$title."', TEXT='".$text."', IMAGE='".$image."', LINK='".$link."', PAGE='".$page."', SECTION='".$section."', COMMENTS='".$comments."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

////////////////////////////////////////////////////////////////
/////////       DELETE Data Using JSON            //////////////
////////////////////////////////////////////////////////////////

function deletePersonWithJSON($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $id = $db->real_escape_string($array[0]['ID']);

    $stmt = $db->prepare("DELETE FROM tbl_people WHERE ID = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}







////////////////////////////////////////////////////////////////
/////////               Login Admin               //////////////
////////////////////////////////////////////////////////////////

function loginAdmin() {

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_users WHERE USERNAME = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['USERNAME'],
                "pass" => $row['PASSWRD']
            );
        }

        $result->free();
        $db->close();
        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        // else
        // {
        //     $_SESSION['login'] = FALSE;
        //     echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
        // }
    }
}

////////////////////////////////////////////////////////////////
/////////              Logout Admin               //////////////
////////////////////////////////////////////////////////////////

function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.html");
    }
}

////////////////////////////////////////////////////////////////
/////////            NON MySQLi Functions         //////////////
////////////////////////////////////////////////////////////////

function two_dates($date1, $date2) {
    $diff = abs(strtotime($date2) - strtotime($date1));
    $years = floor($diff / (365*60*60*24));
    return $years;
}

function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}