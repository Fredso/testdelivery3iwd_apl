// FORGET THIS ONE************************************************************


<?php

include_once('creds.php');

////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

//function get_all_items($myCategory) {
function get_all_items() {
    
    $db = connection();
    //$sql = "SELECT * FROM tbl_content WHERE CATEGORY = '".$myCategory."'";
    $sql = "SELECT * FROM tbl_content";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "idName" => $row['IDNAME'],
            "title" => $row['TITLE'],
            "text" => $row['TEXT'],
            "image" => $row['IMAGE'],
            "link" => $row['LINK']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    $a = substr($json,strpos($json,"["),strlen($json)-strpos($json,"["));
    
    //return $json;
    return $a;
}
?>
