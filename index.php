<?php   
    include_once('functions\functions.php');
    session_start();
    addRecordUsingJSON(); 
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>TITLE HERE</title>
        <link rel="stylesheet" href="" type="text/css" >
    </head>
    <body onload="getDataFromPage()">
        <!-- Content begins here -->
        <header>
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button id="navbarToggleButton" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>                        
                        <a id="homeA" class="navbar-brand" href="#home"><span class="subhead"></span></a>
                    </div><!--navbar-header-->
                    <div class="collapse navbar-collapse" id="collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li id="navbarMenuOption1" class="active"><a href="#featuredCarousel">Home</a></li>
                            <li><a href="#about">About BCS</a></li>
                            <li><a href="#news_resources_information">News & Events</a></li>
                            <li><a href="#news_resources_information">Resources</a></li>
                            <li><a href="#news_resources_information">Information</a></li>
                            <li><a href="#academics">Academics</a></li>
                            <li><a href="#office">Office & Service</a></li>
                            <li>
                                <div class="input-group mysearchbox">
                                    <input type="text"  class="form-control" placeholder="Search"/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                </div>                            
                            </li>
                        </ul>
                    </div><!--collapse navbar-collapse-->
                </div> <!--container-fluid-->
            </nav>

        </header>
        <?php
            // $error_level = error_reporting(0);
            // $dbh = mysql_connect();
            // error_reporting($error_level);
            // $resultRaw = "Warning: mysqli::__construct(): Headers and client library minor version mismatch. Headers:50541 Library:50634 in C:\MAMP\htdocs\mysqli-demo-with-login\functions\functions.php on line 11[{\"id\":\"1\",\"fname\":\"Jeffrey\",\"lname\":\"Kranenburg\",\"dob\":\"1982-10-23\"}]";
            // echo $resultRaw;
            // $result = substr($resultRaw,strpos($resultRaw,"["),strlen($resultRaw)-strpos($resultRaw,"["));
            // echo $result;
            //echo get_all_items();

        ?>
        <div class="container">
            <h1>Content</h1>
            <h2 class="left">Display Web Data</h2>
            <table id="showData">
                <tr>
                    <th>idName</th>
                    <th>Title</th>
                    <th>Text</th>
                    <th>Image</th>
                    <th>Link</th>
                    <th>Page</th>
                    <th>Section</th>
                    <th>Comments</th>
                </tr>
            </table>
        </div>
        <!-- Content ends here -->
        <script src="js\script.js"></script>
    </body>
</html>